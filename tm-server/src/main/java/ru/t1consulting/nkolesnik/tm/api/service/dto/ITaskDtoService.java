package ru.t1consulting.nkolesnik.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDto;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface ITaskDtoService extends IUserOwnedDtoService<TaskDto> {

    void add(@Nullable TaskDto task);

    void add(@Nullable String userId, @Nullable TaskDto task);

    void add(@Nullable Collection<TaskDto> tasks);

    void add(@Nullable String userId, @Nullable Collection<TaskDto> tasks);

    void set(@Nullable Collection<TaskDto> tasks);

    void set(@Nullable String userId, @Nullable Collection<TaskDto> tasks);

    @NotNull
    TaskDto create(@Nullable String userId, @Nullable String name);

    @NotNull
    TaskDto create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    TaskDto create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    long getSize();

    long getSize(@Nullable String userId);

    @NotNull
    List<TaskDto> findAll();

    @NotNull
    List<TaskDto> findAll(@Nullable String userId);

    @NotNull
    List<TaskDto> findAll(@Nullable Comparator<TaskDto> comparator);

    @NotNull
    List<TaskDto> findAll(@Nullable Sort sort);

    @NotNull
    List<TaskDto> findAll(@Nullable String userId, @Nullable Comparator<TaskDto> comparator);

    @NotNull
    List<TaskDto> findAll(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    List<TaskDto> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    TaskDto findById(@Nullable String id);

    @Nullable
    TaskDto findById(@Nullable String userId, @Nullable String id);

    boolean existsById(@Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    void update(@Nullable TaskDto task);

    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    void changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    void clear();

    void clear(@Nullable String userId);

    void remove(@Nullable TaskDto task);

    void remove(@Nullable String userId, @Nullable TaskDto task);

    void removeById(@Nullable String id);

    void removeById(@Nullable String userId, @Nullable String id);

    void removeByProjectId(@Nullable String userId, @Nullable String projectId);

}
