package ru.t1consulting.nkolesnik.tm.api.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import ru.t1consulting.nkolesnik.tm.soap.task.*;

public interface ITaskDtoSoapEndpoint {

    @NotNull
    String NAMESPACE = "http://t1consulting.ru/nkolesnik/tm/soap/task";

    @PayloadRoot(localPart = "TaskCreateRequest", namespace = NAMESPACE)
    TaskCreateResponse create(@NotNull @RequestPayload TaskCreateRequest request);

    @PayloadRoot(localPart = "TaskExistsByIdRequest", namespace = NAMESPACE)
    TaskExistsByIdResponse existsById(@NotNull @RequestPayload TaskExistsByIdRequest request);

    @PayloadRoot(localPart = "TaskFindByIdRequest", namespace = NAMESPACE)
    TaskFindByIdResponse findById(@NotNull @RequestPayload TaskFindByIdRequest request);

    @PayloadRoot(localPart = "TaskFindAllRequest", namespace = NAMESPACE)
    TaskFindAllResponse findAll(@NotNull @RequestPayload TaskFindAllRequest request);

    @PayloadRoot(localPart = "TaskCountRequest", namespace = NAMESPACE)
    TaskCountResponse count(@NotNull @RequestPayload TaskCountRequest request);

    @PayloadRoot(localPart = "TaskSaveRequest", namespace = NAMESPACE)
    TaskSaveResponse save(@NotNull @RequestPayload TaskSaveRequest request);

    @PayloadRoot(localPart = "TaskDeleteRequest", namespace = NAMESPACE)
    TaskDeleteResponse delete(@NotNull @RequestPayload TaskDeleteRequest request);

    @PayloadRoot(localPart = "TaskDeleteByIdRequest", namespace = NAMESPACE)
    TaskDeleteByIdResponse deleteById(@NotNull @RequestPayload TaskDeleteByIdRequest request);

    @PayloadRoot(localPart = "TaskDeleteAllRequest", namespace = NAMESPACE)
    TaskDeleteAllResponse deleteAll(@NotNull @RequestPayload TaskDeleteAllRequest request);

    @PayloadRoot(localPart = "TaskClearRequest", namespace = NAMESPACE)
    TaskClearResponse clear(@NotNull @RequestPayload TaskClearRequest request);

    @PayloadRoot(localPart = "TaskPingRequest", namespace = NAMESPACE)
    TaskPingResponse ping(@NotNull @RequestPayload TaskPingRequest request);

}
