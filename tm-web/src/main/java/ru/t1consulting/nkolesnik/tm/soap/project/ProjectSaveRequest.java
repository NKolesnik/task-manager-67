package ru.t1consulting.nkolesnik.tm.soap.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "project"
})
@XmlRootElement(name = "ProjectSaveRequest")
public class ProjectSaveRequest {

    @XmlElement(required = true)
    protected ProjectDto project;

}
