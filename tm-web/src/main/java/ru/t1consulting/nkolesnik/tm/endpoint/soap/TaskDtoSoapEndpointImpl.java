package ru.t1consulting.nkolesnik.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1consulting.nkolesnik.tm.api.endpoint.soap.ITaskDtoSoapEndpoint;
import ru.t1consulting.nkolesnik.tm.model.dto.TaskDto;
import ru.t1consulting.nkolesnik.tm.service.TaskDtoService;
import ru.t1consulting.nkolesnik.tm.soap.task.*;

import java.util.List;
import java.util.stream.Collectors;

@Endpoint
public class TaskDtoSoapEndpointImpl implements ITaskDtoSoapEndpoint {

    @NotNull
    private static final String NAMESPACE = "http://t1consulting.ru/nkolesnik/tm/soap/task";

    @NotNull
    @Autowired
    private TaskDtoService taskService;

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "TaskCreateRequest", namespace = NAMESPACE)
    public TaskCreateResponse create(@NotNull @RequestPayload TaskCreateRequest request) {
        taskService.create();
        return new TaskCreateResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "TaskExistsByIdRequest", namespace = NAMESPACE)
    public TaskExistsByIdResponse existsById(@NotNull @RequestPayload TaskExistsByIdRequest request) {
        @NotNull final TaskExistsByIdResponse response = new TaskExistsByIdResponse();
        boolean exists = taskService.existsById(request.getId());
        response.setExists(exists);
        return response;
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "TaskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse findById(@NotNull @RequestPayload TaskFindByIdRequest request) {
        @NotNull final TaskFindByIdResponse response = new TaskFindByIdResponse();
        @Nullable final TaskDto task = taskService.findById(request.getId());
        response.setTask(task);
        return response;
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "TaskFindAllRequest", namespace = NAMESPACE)
    public TaskFindAllResponse findAll(@NotNull @RequestPayload TaskFindAllRequest request) {
        @NotNull final TaskFindAllResponse response = new TaskFindAllResponse();
        @Nullable final List<TaskDto> tasks = taskService.findAll().stream().collect(Collectors.toList());
        response.setTask(tasks);
        return response;
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "TaskCountRequest", namespace = NAMESPACE)
    public TaskCountResponse count(@NotNull @RequestPayload TaskCountRequest request) {
        @NotNull final TaskCountResponse response = new TaskCountResponse();
        long count = taskService.count();
        response.setCount(count);
        return response;
    }


    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "TaskSaveRequest", namespace = NAMESPACE)
    public TaskSaveResponse save(@NotNull @RequestPayload TaskSaveRequest request) {
        taskService.save(request.getTask());
        return new TaskSaveResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "TaskDeleteRequest", namespace = NAMESPACE)
    public TaskDeleteResponse delete(@NotNull @RequestPayload TaskDeleteRequest request) {
        taskService.delete(request.getTask());
        return new TaskDeleteResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "TaskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse deleteById(@NotNull @RequestPayload TaskDeleteByIdRequest request) {
        taskService.deleteById(request.getId());
        return new TaskDeleteByIdResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "TaskDeleteAllRequest", namespace = NAMESPACE)
    public TaskDeleteAllResponse deleteAll(@NotNull @RequestPayload TaskDeleteAllRequest request) {
        taskService.deleteAll(request.getTask());
        return new TaskDeleteAllResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "TaskClearRequest", namespace = NAMESPACE)
    public TaskClearResponse clear(@NotNull @RequestPayload TaskClearRequest request) {
        taskService.clear();
        return new TaskClearResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "TaskPingRequest", namespace = NAMESPACE)
    public TaskPingResponse ping(@NotNull @RequestPayload TaskPingRequest request) {
        @NotNull final TaskPingResponse response = new TaskPingResponse();
        response.setMessage("PONG");
        response.setSuccess(true);
        return response;
    }

}
