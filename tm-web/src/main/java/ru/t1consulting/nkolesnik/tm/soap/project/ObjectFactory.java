package ru.t1consulting.nkolesnik.tm.soap.project;

import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;

import javax.xml.bind.annotation.XmlRegistry;


@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }

    public ProjectCreateRequest createProjectCreateRequest() {
        return new ProjectCreateRequest();
    }

    public ProjectCreateResponse createProjectCreateResponse() {
        return new ProjectCreateResponse();
    }

    public ProjectExistsByIdRequest createProjectExistsByIdRequest() {
        return new ProjectExistsByIdRequest();
    }

    public ProjectExistsByIdResponse createProjectExistsByIdResponse() {
        return new ProjectExistsByIdResponse();
    }

    public ProjectFindByIdRequest createProjectFindByIdRequest() {
        return new ProjectFindByIdRequest();
    }

    public ProjectFindByIdResponse createProjectFindByIdResponse() {
        return new ProjectFindByIdResponse();
    }

    public ProjectDto createProjectDto() {
        return new ProjectDto();
    }

    public ProjectFindAllRequest createProjectFindAllRequest() {
        return new ProjectFindAllRequest();
    }

    public ProjectFindAllResponse createProjectFindAllResponse() {
        return new ProjectFindAllResponse();
    }

    public ProjectCountRequest createProjectCountRequest() {
        return new ProjectCountRequest();
    }

    public ProjectCountResponse createProjectCountResponse() {
        return new ProjectCountResponse();
    }

    public ProjectSaveRequest createProjectSaveRequest() {
        return new ProjectSaveRequest();
    }

    public ProjectSaveResponse createProjectSaveResponse() {
        return new ProjectSaveResponse();
    }

    public ProjectDeleteRequest createProjectDeleteRequest() {
        return new ProjectDeleteRequest();
    }

    public ProjectDeleteResponse createProjectDeleteResponse() {
        return new ProjectDeleteResponse();
    }

    public ProjectDeleteByIdRequest createProjectDeleteByIdRequest() {
        return new ProjectDeleteByIdRequest();
    }

    public ProjectDeleteByIdResponse createProjectDeleteByIdResponse() {
        return new ProjectDeleteByIdResponse();
    }

    public ProjectDeleteAllRequest createProjectDeleteAllRequest() {
        return new ProjectDeleteAllRequest();
    }

    public ProjectDeleteAllResponse createProjectDeleteAllResponse() {
        return new ProjectDeleteAllResponse();
    }

    public ProjectClearRequest createProjectClearRequest() {
        return new ProjectClearRequest();
    }

    public ProjectClearResponse createProjectClearResponse() {
        return new ProjectClearResponse();
    }

    public ProjectPingRequest createProjectPingRequest() {
        return new ProjectPingRequest();
    }

    public ProjectPingResponse createProjectPingResponse() {
        return new ProjectPingResponse();
    }

}
