package ru.t1consulting.nkolesnik.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1consulting.nkolesnik.tm.api.endpoint.soap.IProjectDtoSoapEndpoint;
import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;
import ru.t1consulting.nkolesnik.tm.service.ProjectDtoService;
import ru.t1consulting.nkolesnik.tm.soap.project.*;

import java.util.List;
import java.util.stream.Collectors;

@Endpoint
public class ProjectDtoSoapEndpointImpl implements IProjectDtoSoapEndpoint {

    @NotNull
    private static final String NAMESPACE = "http://t1consulting.ru/nkolesnik/tm/soap/project";

    @NotNull
    @Autowired
    private ProjectDtoService projectService;

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "ProjectCreateRequest", namespace = NAMESPACE)
    public ProjectCreateResponse create(@NotNull @RequestPayload ProjectCreateRequest request) {
        projectService.create();
        return new ProjectCreateResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "ProjectExistsByIdRequest", namespace = NAMESPACE)
    public ProjectExistsByIdResponse existsById(@NotNull @RequestPayload ProjectExistsByIdRequest request) {
        @NotNull final ProjectExistsByIdResponse response = new ProjectExistsByIdResponse();
        boolean exists = projectService.existsById(request.getId());
        response.setExists(exists);
        return response;
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "ProjectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findById(@NotNull @RequestPayload ProjectFindByIdRequest request) {
        @NotNull final ProjectFindByIdResponse response = new ProjectFindByIdResponse();
        @Nullable final ProjectDto project = projectService.findById(request.getId());
        response.setProject(project);
        return response;
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "ProjectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse findAll(@NotNull @RequestPayload ProjectFindAllRequest request) {
        @NotNull final ProjectFindAllResponse response = new ProjectFindAllResponse();
        @Nullable final List<ProjectDto> projects = projectService.findAll().stream().collect(Collectors.toList());
        response.setProject(projects);
        return response;
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "ProjectCountRequest", namespace = NAMESPACE)
    public ProjectCountResponse count(@NotNull @RequestPayload ProjectCountRequest request) {
        @NotNull final ProjectCountResponse response = new ProjectCountResponse();
        long count = projectService.count();
        response.setCount(count);
        return response;
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "ProjectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse save(
            @NotNull
            @RequestPayload final ProjectSaveRequest request
    ) {
        projectService.save(request.getProject());
        return new ProjectSaveResponse();
    }


    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "ProjectDeleteRequest", namespace = NAMESPACE)
    public ProjectDeleteResponse delete(@NotNull @RequestPayload ProjectDeleteRequest request) {
        projectService.delete(request.getProject());
        return new ProjectDeleteResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "ProjectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(@NotNull @RequestPayload ProjectDeleteByIdRequest request) {
        projectService.deleteById(request.getId());
        return new ProjectDeleteByIdResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "ProjectDeleteAllRequest", namespace = NAMESPACE)
    public ProjectDeleteAllResponse deleteAll(@NotNull @RequestPayload ProjectDeleteAllRequest request) {
        projectService.deleteAll(request.getProject());
        return new ProjectDeleteAllResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "ProjectClearRequest", namespace = NAMESPACE)
    public ProjectClearResponse clear(@NotNull @RequestPayload ProjectClearRequest request) {
        projectService.clear();
        return new ProjectClearResponse();
    }

    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "ProjectPingRequest", namespace = NAMESPACE)
    public ProjectPingResponse ping(@NotNull @RequestPayload ProjectPingRequest request) {
        @NotNull final ProjectPingResponse response = new ProjectPingResponse();
        response.setMessage("PONG");
        response.setSuccess(true);
        return response;
    }

}
